This repository contains the results of a social engineering study performed with funds from NSF Award #1813858, "SaTC: CORE: Small: Detecting Social Engineering Attacks Using Semantic Language Analysis". The PI of this project is Professor Ian G. Harris, Department of Computer Science, University of California Irvine.

Participants in the study were UCI students who received telephone-based social engineering attacks using one of 27 different attack scripts. All attacsk phone calls were recorded, anonymized, and transcribed. In order to ensure informed consent, each participant was informed that they would be attacked within 3 months time. The scripts varied several parameters of the attacks including the pretext used, the elicitation method, and the goal of the attack. In spite of the fact that participants were notified that they would be attacked, a large fraction of the participants were deceived by the scam.
 
This reposity contains several directories and files.

-- ScriptInfo.xlsx spreadsheet

This directory contains 27 .docx file containing the text of each script used in the attacks. These scripts were used as the bases for the attacks but the text of these scripts differs from the transcripts of the actual attacks, which include significant variation. Each script is named "Script_X.docx", where X is the number of the script.

-- Audio Recordings directory

This directory contains .mp3 audio files which are the recordings of the attack phone calls. The files are all named "X.mp3", where X is the number of the call. The number sequence contains some gaps corresponding to participants who never answered their phone.

-- Transcripts directory

This directory contains .docx files which are the transcripts of the attack phone calls. The files are all named "X.docx", where X is the number of the call.

-- CallInfo.xlsx spreadsheet

This spreadsheet contains information about each attack phone call. There is one row for each phone call. The table contains the following 4 columns. 

-Call Number - This is the number of the call (matching numbers in used in the Transcripts and Audio Recordings directories).
-Script Number - This is the number of the attack script that was used.
-Time Frame - This is a "0" to indicate 2 months or more, and a "1" to indicate 1-2 months.
-Success? - This is a "0" to indicate that the attack failed, and a "1" to indicate that the attack succeeded.

-- Script Templates directory

This contains information about each script. There are 27 rows, one for each script. The table contains the following 4 columns.

-Script Number - This is the numberof the script, 1-27.
-Pretext Number - This is the number of the pretext, 1-3.
-Elicitation Number - This is the number of the elicitation, numbered X.Y. 
-Information Goal Number - This is the number of the goal, numbered X.Y. 

**Reference**

If you publish any work making use of this data, we ask you to cite the following task description paper:

I. G. Harris, A. Derakhshan, and M. Carlsson "A Study of Targeted Telephone Scams Involving Live Attackers" , Socio-Technical Aspects in Security and Trust (STAST) , 2021. 
